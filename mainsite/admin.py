from django.contrib import admin
from .models import Post

# Register your models here.


class PostAdmin(admin.ModelAdmin):
    """重写Post显示方式的类，以显示除标题外的网址和时间日期信息"""
    list_display = ('title', 'slug', 'pub_date')


admin.site.register(Post, PostAdmin)
