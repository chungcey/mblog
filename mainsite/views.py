from django.shortcuts import render
from .models import Post
from datetime import datetime
from django.shortcuts import redirect

# Create your views here.


def homepage(request):
    # 主页
    posts = Post.objects.all()
    now = datetime.now()
    return render(request, 'index.html', locals())


def show_post(request, slug):
    # 文章内容
    try:
        post = Post.objects.get(slug=slug)
        if post is not None:
            return render(request, 'post.html', locals())
    except:
        return redirect('/')  # 返回主页



