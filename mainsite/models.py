from django.db import models
from django.utils import timezone

# Create your models here.


class Post(models.Model):
    """定义博客表数据模型"""
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)   # 本文网址
    body = models.TextField()
    pub_date = models.DateTimeField(default=timezone.now)  # 本文发表时间

    class Meta:
        ordering = ("-pub_date",)   # 文章显示顺序以发表时间为依据

    def __str__(self):
        """以文章标题前50个字符作为显示内容"""
        return self.title[:51]

